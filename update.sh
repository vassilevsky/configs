#!/bin/bash

for f in *.conf
do
    ln -sfv $PWD/$f /etc/webkaos/conf.d/$f
done

service webkaos reload
